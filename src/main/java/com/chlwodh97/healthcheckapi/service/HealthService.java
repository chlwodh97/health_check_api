package com.chlwodh97.healthcheckapi.service;


import com.chlwodh97.healthcheckapi.entity.Health;
import com.chlwodh97.healthcheckapi.model.*;
import com.chlwodh97.healthcheckapi.repository.HealthRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cglib.core.Local;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class HealthService {
    private final HealthRepository healthRepository;

    public void setHealth(HealthRequest request) {
        Health addData = new Health();
        addData.setDateCreate(LocalDate.now());
        addData.setName(request.getName());
        addData.setHealthStatus(request.getHealthStatus());
        addData.setEtcMemo(request.getEtcMemo());
        addData.setIsChronicDisease(request.getIsChronicDisease());

        healthRepository.save(addData);
    }

    public List<HealthItem> getHealths() {
        List<Health> originList = healthRepository.findAll();

        List<HealthItem> result = new LinkedList<>();

        for (Health health : originList) {
            HealthItem addItem = new HealthItem();
            addItem.setId(health.getId());
            addItem.setDateCreate(health.getDateCreate());
            addItem.setName(health.getName());
            addItem.setHealthStatus(health.getHealthStatus().getName());
            addItem.setIsGoHome(health.getHealthStatus().getIsGoHome());

            result.add(addItem);
        }

        return result;
    }

    public HealthResponse getHealth(long id) {
        Health originData = healthRepository.findById(id).orElseThrow();

        HealthResponse response = new HealthResponse();
        response.setId(originData.getId());
        response.setHealthStatusName(originData.getHealthStatus().getName());


        response.setIsGoHomeName(originData.getHealthStatus().getIsGoHome() ? "예" : "아니오");

        response.setIsChronicDiseaseName(originData.getIsChronicDisease() ? "예" : "아니오");


        response.setName(originData.getName());
        response.setDateCreate(originData.getDateCreate());


        return response;
    }

    public void putHealthStatus(long id , HealthStatusChangeRequest request) {
        Health orginData = healthRepository.findById(id).orElseThrow();

        orginData.setHealthStatus(request.getHealthStatus());

        healthRepository.save(orginData);
    }

    public void putBaseInfo(long id , HealthBaseInfoChangeRequest request) {
        Health orginData = healthRepository.findById(id).orElseThrow();

        orginData.setName(request.getName());
        orginData.setIsChronicDisease(request.getIsChronicDisease());

        healthRepository.save(orginData);
    }
}
