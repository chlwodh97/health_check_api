package com.chlwodh97.healthcheckapi.eums;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MachineType {

    RUNIING("런닝머신" , 700D)
    , WEIGHT("웨이트" , 200D)
    ;

    private final String name;
    private final Double pressKg;
}
