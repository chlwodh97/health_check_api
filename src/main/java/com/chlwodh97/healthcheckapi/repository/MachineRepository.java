package com.chlwodh97.healthcheckapi.repository;

import com.chlwodh97.healthcheckapi.entity.Machine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MachineRepository extends JpaRepository<Machine , Long> {
}
