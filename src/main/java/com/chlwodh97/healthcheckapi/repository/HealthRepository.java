package com.chlwodh97.healthcheckapi.repository;

import com.chlwodh97.healthcheckapi.entity.Health;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HealthRepository extends JpaRepository<Health , Long> {

}
