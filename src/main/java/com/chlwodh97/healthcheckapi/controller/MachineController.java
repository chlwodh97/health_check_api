package com.chlwodh97.healthcheckapi.controller;


import com.chlwodh97.healthcheckapi.model.MachineRequest;
import com.chlwodh97.healthcheckapi.model.MachineStaticsResponse;
import com.chlwodh97.healthcheckapi.service.MachineService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/machine")
public class MachineController {
    private final MachineService machineService;

    @PostMapping("/new")
    public String setMachine(@RequestBody MachineRequest request){

        machineService.setMachine(request);

        return "ok ok";
    }

    @GetMapping("/statics")
    public MachineStaticsResponse getStatics(){
        return machineService.getStatics();
    }
}
