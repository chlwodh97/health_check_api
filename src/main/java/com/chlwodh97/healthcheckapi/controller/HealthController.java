package com.chlwodh97.healthcheckapi.controller;


import com.chlwodh97.healthcheckapi.model.*;
import com.chlwodh97.healthcheckapi.service.HealthService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/health")
public class HealthController {
    private final HealthService healthService;

    @PostMapping("/people")
    public String setHealth(@RequestBody HealthRequest request) {
        healthService.setHealth(request);

        return "OK OK";
    }

    @GetMapping("/all")
    public List<HealthItem> getHealths() {

        return healthService.getHealths();
    }

    @GetMapping("/detail/{id}")
    public HealthResponse getHealth(@PathVariable long id){

        return healthService.getHealth(id);
    }

    @PutMapping("/status/{id}")
    public String putHealthStatus(@PathVariable long id , @RequestBody HealthStatusChangeRequest request){
        healthService.putHealthStatus(id, request);

        return "ok";
    }

    @PutMapping("base-info/{id}")
    public String putBaseInfo(@PathVariable long id , @RequestBody HealthBaseInfoChangeRequest request){
        healthService.putBaseInfo(id, request);

        return  "ok ok";
    }
}
